package com.example.truyentranhhapdan;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Util {
    public static boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            // edited, to include @Arthur's comment
            // e.g. in case JSONArray is valid as well...
            try {
                new JSONArray(test);
            } catch (JSONException ex1) {
                return false;
            }
        }
        return true;
    }
    public static void Typeface(TextView textView, Context context){
        Typeface typeface=Typeface.createFromAsset(context.getAssets(),"HelloOlivia.ttf");
        textView.setTypeface(typeface);
    }
}
