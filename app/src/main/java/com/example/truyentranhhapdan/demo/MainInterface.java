package com.example.truyentranhhapdan.demo;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface MainInterface {

    @POST("gettusach.php")
    @FormUrlEncoded
    Call<String> STRING_CALL(
            @Field("page") int page,
            @Field("limit") int limit,
            @Field("iduser") int iduser
    );
    @POST("trangtruyen.php")
    @FormUrlEncoded
    Call<String> STRING_CALLS(
            @Field("page") int page,
            @Field("limit") int limit,
            @Field("idchuong") int idchuong
    );
    @POST("gettruyentheloai.php")
    @FormUrlEncoded
    Call<String> STRING_CALLS_THELOAI(
            @Field("page") int page,
            @Field("limit") int limit,
            @Field("idtheloai") int idtheloai
    );
    @Multipart
    @POST("uploadfile.php")
    Call<String> UPLOAD_PHOTO(
            @Part MultipartBody.Part photo,
            @Part("iduser") RequestBody iduser
            );
    @POST("updateuser.php")
    @FormUrlEncoded
    Call<String> STRING_CALL_UPDATE_USER(
            @Field("iduser") int iduser,
            @Field("email") String email,
            @Field("password") String password
    );
}
