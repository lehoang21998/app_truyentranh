package com.example.truyentranhhapdan.activity;

import android.os.Bundle;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.truyentranhhapdan.R;
import com.example.truyentranhhapdan.adapter.Adaptertruyen;
import com.example.truyentranhhapdan.demo.MainInterface;
import com.example.truyentranhhapdan.model.TruyenTranh;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static com.example.truyentranhhapdan.activity.MainActivity.userschinh;


public class Fragment_tusach extends Fragment {
    private NestedScrollView nestedScrollView;
    private View view;
    private RecyclerView recyclerView;
    private TextView txtthongbao;
    private int iduser=userschinh.getId();

    public static Adaptertruyen adaptertruyen;
    public static ArrayList<TruyenTranh> arrayListTruyen;

    private LinearLayout linearLayout;
    private LinearLayoutManager linearLayoutManager;
    private int page=1;
    private int limit=8;


    public Fragment_tusach() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        arrayListTruyen.clear();
        getData(page,limit,iduser);
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(scrollY==v.getChildAt(0).getMeasuredHeight()-v.getMeasuredHeight()){
                    page++;
                    linearLayout.setVisibility(View.VISIBLE);
                    getData(page,limit,iduser);
                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_tusach, container, false);
        AnhXa(view);
        return view;
    }

    private void getData(int page,int limit,int iduser) {
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl("http://thietbiandroid.000webhostapp.com/")
                .addConverterFactory(ScalarsConverterFactory.create()).build();
        MainInterface mainInterface=retrofit.create(MainInterface.class);
        Call<String> call =mainInterface.STRING_CALL(page,limit,iduser);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()&&response.body()!=null){
                    linearLayout.setVisibility(View.GONE);
                    Log.d("LOG",response.body().toString());
                    try {
                        JSONArray jsonArray=new JSONArray(response.body());
                        parseResulf(jsonArray);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("LOG",t.getMessage());
            }
        });
    }

    private void parseResulf(JSONArray jsonArray) {
        for(int i=0;i<jsonArray.length();i++){
            try {
                JSONObject jsonObject=jsonArray.getJSONObject(i);
                             int idtruyen =jsonObject.getInt("id");
                            String tentruyen=jsonObject.getString("tentruyen");
                            String tacgia=jsonObject.getString("tacgia");
                            String hinhanh=jsonObject.getString("hinhanh");
                            String mota=jsonObject.getString("mota");
                            int sochuong =jsonObject.getInt("sochuong");
                            long ratting =jsonObject.getInt("ratting");
                            int idtheloai =jsonObject.getInt("idtheloai");
                            String tentheloai=jsonObject.getString("tentheloai");
                            TruyenTranh truyenTranh = new TruyenTranh(idtruyen,tentruyen,tacgia,hinhanh,mota,sochuong,ratting,idtheloai,tentheloai);
                            arrayListTruyen.add(truyenTranh);
                            adaptertruyen.setData(arrayListTruyen);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    private void AnhXa(View view) {
        nestedScrollView=view.findViewById(R.id.scoll_view);
        recyclerView=view.findViewById(R.id.recleviewtusach);
//        txtthongbao=view.findViewById(R.id.txtthongbaotusach);

        linearLayout=view.findViewById(R.id.line1);

        arrayListTruyen=new ArrayList<>();
        linearLayoutManager=new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        adaptertruyen=new Adaptertruyen(getContext(),R.layout.item_truyen_tranh_tusach);
        adaptertruyen.setData(arrayListTruyen);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        recyclerView.setAdapter(adaptertruyen);
    }
}