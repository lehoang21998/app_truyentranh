package com.example.truyentranhhapdan.activity;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.truyentranhhapdan.R;


public class Fragment_khampha extends Fragment {
    private View view;
    public Fragment_khampha() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment_khampha, container, false);
        return view;
    }
}