package com.example.truyentranhhapdan.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.truyentranhhapdan.R;
import com.example.truyentranhhapdan.demo.APIUtils;
import com.example.truyentranhhapdan.demo.MainInterface;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.LOCATION_SERVICE;
import static com.example.truyentranhhapdan.activity.MainActivity.userschinh;


public class Fragment_Docgia extends Fragment {

    private static final String basic_url="https://thietbiandroid.000webhostapp.com/";
    private ProgressBar progressBar;
    private View mview;
    private CircleImageView circleImageView;
    private TextView txtfullname,txtTaiKhoan,txtemail,txtChinhsua,txtDangxuat;
    private int REQUEST_CODE_IMAGES=123;
    private String RealPath="";
    //getdata doc gia
    private int  iduser=userschinh.getId();
    private String fullname=userschinh.getFullname();
    private String tentaikhoan=userschinh.getUsername();
    private String password=userschinh.getPassword();
    private String email=userschinh.getEmail();
    private String hinhanh=userschinh.getHinhanh();


    private int STORAGE_PERMISSION_CODE = 1;


    public Fragment_Docgia() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mview= inflater.inflate(R.layout.fragment__docgia, container, false);
        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getContext(), "You have already granted this permission!",
                    Toast.LENGTH_SHORT).show();
        } else {
            requestStoragePermission();
        }
        AnhXa();
        if(hinhanh.equals("")){
            Log.d("hinhanh","ẢNH RỖNG");
        }else {
            Glide
                    .with(getContext())
                    .load(hinhanh)
                    .centerCrop()
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.error)
                    .into(circleImageView);
        }
        return mview;
    }
    @Override
    public void onResume() {
        super.onResume();
        Log.d("HASTAG", " DOC GIA ON REUSUME");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("HASTAG", " DOC GIA ON onPause");
    }
    private void AnhXa() {
        txtTaiKhoan=mview.findViewById(R.id.txttentaikhoan);
        txtTaiKhoan.setText(fullname);

        txtemail =mview.findViewById(R.id.txtemail);
        txtemail.setText(email);

        //CHỈNH SỮA
        txtChinhsua=mview.findViewById(R.id.txtUpdate);
        txtChinhsua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogUpdate();
            }
        });

        //ĐĂNG XUẤT
        txtDangxuat=mview.findViewById(R.id.txtSignOut);
        txtDangxuat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getContext(),SignIn.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
        progressBar=mview.findViewById(R.id.progress_circular_docgia);
        circleImageView=mview.findViewById(R.id.imageavatar);
        txtfullname=mview.findViewById(R.id.txtfullname);
        txtfullname.setText(fullname);
        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,REQUEST_CODE_IMAGES);
            }
        });
    }
    private void DialogUpdate(){
        final Dialog dialog=new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_updateuser);
        //Anh xa
        final EditText editmail,editmkc,editmkm,editsothich;
        Button btnHuy,btnDongY;
        editmail=dialog.findViewById(R.id.edit_email);
        editmkc=dialog.findViewById(R.id.edit_mkc);
        editmkm=dialog.findViewById(R.id.edit_mkm);
        btnHuy=dialog.findViewById(R.id.btnhuy);
        btnDongY=dialog.findViewById(R.id.btndony);
        editmail.setText(email);
        btnHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        btnDongY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email=editmail.getText().toString();
                String passwordm=editmkm.getText().toString();
                String passwordc=editmkc.getText().toString();
                String pass2=SignIn.password;
                Log.d("UDE",pass2);
                if(!passwordc.equals(pass2)){
                    Toast.makeText(getContext(),"Mật khẩu cũ không đúng",Toast.LENGTH_LONG).show();
                }else{
                    if(!isValueEmail(email) || !isValuePassword(passwordm)){
                        Toast.makeText(getContext(),"Email không đúng định dạng ,hoặc mất khẩu mức thấp",Toast.LENGTH_LONG).show();
                    }else{
                        Retrofit retrofit=new Retrofit.Builder()
                                .baseUrl("http://thietbiandroid.000webhostapp.com/")
                                .addConverterFactory(ScalarsConverterFactory.create()).build();
                        MainInterface mainInterface=retrofit.create(MainInterface.class);
                        Call<String> call=mainInterface.STRING_CALL_UPDATE_USER(iduser,email,passwordm);
                        call.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                String messge=response.body();
                                if(messge.equals("UPDATE THANH CONG")){
                                    Toast.makeText(getContext(),"UPDATE THÀNH CÔNG",Toast.LENGTH_LONG).show();
                                    Thread thread=new Thread();
                                    try {
                                        thread.sleep(3000);
                                        dialog.dismiss();
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }else if(messge.equals("UPDATE THAT BAI")){
                                    Toast.makeText(getContext(),"LỖI HỆ THỐNG",Toast.LENGTH_LONG).show();
                                }
                                else if(messge.equals("EMAIL DA TON TAI")){
                                    Toast.makeText(getContext(),"EMAIL ĐÃ SỬ DỤNG",Toast.LENGTH_LONG).show();
                                }else if(messge.equals("KHONG TIM THAY DU LIEU")){
                                    Toast.makeText(getContext(),"SEVER KHÔNG TÌM THẤY THÔNG TIN",Toast.LENGTH_LONG).show();
                                }else  Toast.makeText(getContext(),"LỖI HỆ THỐNG",Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {
                                Log.d("UDEe",t.getMessage());
                                Toast.makeText(getContext(),"LỖI HỆ THỐNG ERRORRRRRRRRRRRRRRRRRRRRRR",Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                }
            }
        });
        dialog.show();
    }
    private   boolean isValueEmail(String email){
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    public boolean isValuePassword(String password){
        return !TextUtils.isEmpty(password) &&password.length()>=6;
    }
    private void requestStoragePermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) getContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE)) {
            new AlertDialog.Builder(getContext())
                    .setTitle("Permission needed")
                    .setMessage("This permission is needed because of this and that")
                    .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions((Activity) getContext(),
                                    new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
                        }
                    })
                    .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create().show();
        } else {
            ActivityCompat.requestPermissions((Activity) getContext(),
                    new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == STORAGE_PERMISSION_CODE)  {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getContext(), "Permission GRANTED", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getContext(), "Permission DENIED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==REQUEST_CODE_IMAGES && resultCode==RESULT_OK && data!=null){
            Uri uri=data.getData();
            RealPath=getRealPathFromURI(uri);
            try {
                InputStream inputStream = getActivity().getContentResolver().openInputStream(uri);
                Bitmap bitmap= BitmapFactory.decodeStream(inputStream);
                UpdateAvartar(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void UpdateAvartar(final Bitmap bitmap) {
        final AlertDialog.Builder alerdiglog=new AlertDialog.Builder(getContext());
        alerdiglog.setTitle("UPDATE MY IMAGE");
        alerdiglog.setIcon(R.drawable.iconupdate);
        alerdiglog.setMessage("Bạn muốn cập nhập ảnh này ?");
        alerdiglog.setPositiveButton("Có", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                circleImageView.setVisibility(View.INVISIBLE);
                progressBar.setVisibility(View.VISIBLE);

                File file=new File(RealPath);
                String file_path=file.getAbsolutePath();
                String mangtenfile[]=file_path.split("\\.");
                file_path=mangtenfile[0] + System.currentTimeMillis()+ "."+mangtenfile[1];
                RequestBody requestBody=RequestBody.create(MediaType.parse("multipart/form-data"),file);
                RequestBody liduser=RequestBody.create(MediaType.parse("text/plain"), String.valueOf(iduser));
                MultipartBody.Part body=MultipartBody.Part.createFormData("uploadfile",file_path,requestBody);
                MainInterface mainInterface= APIUtils.getData();
                Call<String> call=mainInterface.UPLOAD_PHOTO(body,liduser);
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        String messge=response.body();
                        Log.d("LOG_mes",messge);
                        if(response.isSuccessful() || messge.length()>0){
                           if(messge.equals("thanh cong")){
                               progressBar.setVisibility(View.INVISIBLE);
                               Toast.makeText(getContext(),"UPDATE Image thành công",Toast.LENGTH_LONG).show();
                               circleImageView.setImageBitmap(bitmap);
                               circleImageView.setVisibility(View.VISIBLE);
                           }
                           else{
                               progressBar.setVisibility(View.INVISIBLE);
                               circleImageView.setVisibility(View.VISIBLE);
                               Toast.makeText(getContext(),"UPDATE Image thất bại",Toast.LENGTH_LONG).show();
                           }
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.d("LOG_AERROR",t.getMessage());
                    }
                });



            }
        });
        alerdiglog.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        alerdiglog.show();
    }
    public String getRealPathFromURI (Uri contentUri) {
        String path = null;
        String[] proj = { MediaStore.MediaColumns.DATA };
        Cursor cursor = getContext().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
        }
        cursor.close();
        return path;
    }
}