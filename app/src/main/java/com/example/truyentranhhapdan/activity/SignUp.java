package com.example.truyentranhhapdan.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.truyentranhhapdan.R;
import com.example.truyentranhhapdan.model.Users;
import com.google.android.material.textfield.TextInputEditText;
import com.vishnusivadas.advanced_httpurlconnection.PutData;

public class SignUp extends AppCompatActivity implements View.OnClickListener{
    private TextInputEditText editfullname,editusername,editpass,editemail;
    private Button btnSingUp;
    private ProgressBar progressBar;
    private TextView txtSignIn,txtThongBao,txtthongbaopass,txtthongbaoemail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        AnhXa();
    }


    private void AnhXa() {
        txtthongbaopass=findViewById(R.id.txtthongpass);
        txtthongbaoemail=findViewById(R.id.txtthongmail);
        editfullname=findViewById(R.id.edit_fullname);
        editusername=findViewById(R.id.edit_username);
        editpass=findViewById(R.id.edit_password);
        editemail=findViewById(R.id.edit_email);
        btnSingUp=findViewById(R.id.btnSignUp);
        progressBar=findViewById(R.id.progress_circular);
        txtSignIn=findViewById(R.id.txtSignIn);
        txtThongBao=findViewById(R.id.txtThongbaoSignUp);
        btnSingUp.setOnClickListener(this);
        txtSignIn.setOnClickListener(this);
        editfullname.setOnClickListener(this);
        editusername.setOnClickListener(this);
        editemail.setOnClickListener(this);
        editpass.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.edit_password:
                txtthongbaopass.setVisibility(View.GONE);break;
            case R.id.edit_email:
                txtthongbaoemail.setVisibility(View.GONE);break;
            case R.id.btnSignUp:
                final String fullname,username,password,email;
                fullname=editfullname.getText().toString();
                username=editusername.getText().toString();
                password=editpass.getText().toString();
                email=editemail.getText().toString();
                final Users users=new Users(fullname,username,password,email);

                if(!users.getFullname().equals("") && !users.getUsername().equals("") &&users.isValueEmail()&&users.isValuePassword()){
                    progressBar.setVisibility(View.VISIBLE);
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            //Starting Write and Read data with URL
                            //Creating array for parameters
                            String[] field = new String[4];
                            field[0] = "fullname";
                            field[1] = "username";
                            field[2] = "password";
                            field[3] = "email";
                            //Creating array for data
                            String[] data = new String[4];
                            data[0] = fullname;
                            data[1] = username;
                            data[2] = password;
                            data[3] = email;
                            PutData putData = new PutData("https://thietbiandroid.000webhostapp.com/signup.php", "POST", field, data);
                            if (putData.startPut()) {
                                if (putData.onComplete()) {
                                    progressBar.setVisibility(View.GONE);
                                    String result = putData.getResult();
                                    Log.d("AAA",result);
                                    if (result.equals("Sign Up Success")){
                                        Intent intent=new Intent(getApplicationContext(),SignIn.class);
                                        startActivity(intent);
                                        finish();
                                    }else{
                                        txtThongBao.setVisibility(View.VISIBLE);
                                        txtThongBao.setText("Đăng kí không thành công,tên tài khoản đã tồn tại");
                                    }
                                }
                            }
                        }
                    });
                }
                else {
                    if(!users.isValuePassword()){
                        txtthongbaopass.setVisibility(View.VISIBLE);
                        txtthongbaopass.setText("Mật khẩu phải lớn hơn 5 ký tự");
                        editpass.setText("");
                    }
                    if(!users.isValueEmail()){
                        txtthongbaoemail.setVisibility(View.VISIBLE);
                        txtthongbaoemail.setText("email không hợp lệ");
                        editemail.setText("");
                    }
                    txtThongBao.setVisibility(View.VISIBLE);
                    txtThongBao.setText("Không được bỏ trống thông tin nào!!!");
                }
                break;
            case R.id.txtSignIn:
                Intent intent=new Intent(getApplicationContext(),SignIn.class);
                startActivity(intent);
                finish();
                break;
        }
    }
}