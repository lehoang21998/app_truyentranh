package com.example.truyentranhhapdan.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.example.truyentranhhapdan.R;
import com.example.truyentranhhapdan.adapter.Adaptertruyen;
import com.example.truyentranhhapdan.demo.MainInterface;
import com.example.truyentranhhapdan.model.TruyenTranh;
import com.example.truyentranhhapdan.model.theloai;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class TrangCuaTheLoai extends AppCompatActivity {
    private int idtheloai=-1;
    private String tentheloai="";
    private RecyclerView recyclerView_theloai;
    private LinearLayout linearLayout_loadmore;
    private NestedScrollView nestedScrollView;

    private Toolbar toolbar;

    private ArrayList<TruyenTranh> listtruyen;
    private Adaptertruyen adapterListtruyen;

    private int page=1;
    private int limit=8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trang_cua_the_loai);
        getThongTinTheLoai();
        AnhXa();
        ActionBar();
        getData(page,limit,idtheloai);
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(scrollY==v.getChildAt(0).getMeasuredHeight()-v.getMeasuredHeight()){
                    page++;
                    linearLayout_loadmore.setVisibility(View.VISIBLE);
                    getData(page,limit,idtheloai);
                }
            }
        });
    }
    private void ActionBar(){
       toolbar.setTitle(tentheloai);
        toolbar.setNavigationIcon(R.drawable.backicon);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void AnhXa() {
        recyclerView_theloai=findViewById(R.id.recleviewtheloai);
        linearLayout_loadmore=findViewById(R.id.loadmore);
        nestedScrollView=findViewById(R.id.scoll_view_theloai);
        toolbar=findViewById(R.id.toobar_theloai);


        // set recyclerview_theloai
        listtruyen=new ArrayList<>();
        adapterListtruyen=new Adaptertruyen(TrangCuaTheLoai.this,R.layout.item_truyen_tranh_tusach);
        recyclerView_theloai.setHasFixedSize(true);
        recyclerView_theloai.setLayoutManager(new GridLayoutManager(getApplicationContext(),2));
        recyclerView_theloai.setAdapter(adapterListtruyen);


    }

    private void getThongTinTheLoai() {
        theloai datatheloai= (theloai) getIntent().getSerializableExtra("ThongTinTheLoai");
        tentheloai=datatheloai.getTenTheLoai();
        idtheloai=datatheloai.getIdtheloai();
    }

    private void getData(int page,int limit,int idtheloai) {
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl("http://thietbiandroid.000webhostapp.com/")
                .addConverterFactory(ScalarsConverterFactory.create()).build();
        MainInterface mainInterface=retrofit.create(MainInterface.class);
        Call<String> call =mainInterface.STRING_CALLS_THELOAI(page,limit,idtheloai);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()&&response.body()!=null){
                    linearLayout_loadmore.setVisibility(View.GONE);
                    Log.d("LOG_TL",response.body().toString());
                    try {
                        JSONArray jsonArray=new JSONArray(response.body());
                        parseResulf(jsonArray);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("LOG",t.getMessage());
            }
        });
    }

    private void parseResulf(JSONArray jsonArray) {
        for(int i=0;i<jsonArray.length();i++){
            try {
                JSONObject jsonObject=jsonArray.getJSONObject(i);
                int idtruyen =jsonObject.getInt("id");
                String tentruyen=jsonObject.getString("tentruyen");
                String tacgia=jsonObject.getString("tacgia");
                String hinhanh=jsonObject.getString("hinhanh");
                String mota=jsonObject.getString("mota");
                int sochuong =jsonObject.getInt("sochuong");
                long ratting =jsonObject.getInt("ratting");
                int idtheloai =jsonObject.getInt("idtheloai");
                String tentheloai=jsonObject.getString("tentheloai");
                TruyenTranh truyenTranh = new TruyenTranh(idtruyen,tentruyen,tacgia,hinhanh,mota,sochuong,ratting,idtheloai,tentheloai);
                listtruyen.add(truyenTranh);
                adapterListtruyen.setData(listtruyen);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}