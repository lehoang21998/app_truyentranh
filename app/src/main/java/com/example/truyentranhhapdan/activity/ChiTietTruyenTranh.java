package com.example.truyentranhhapdan.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.truyentranhhapdan.R;
import com.example.truyentranhhapdan.Util;
import com.example.truyentranhhapdan.adapter.AdapterChuong;
import com.example.truyentranhhapdan.model.ChuongTruyen;
import com.example.truyentranhhapdan.model.TruyenTranh;
import com.example.truyentranhhapdan.model.theloai;
import com.vishnusivadas.advanced_httpurlconnection.PutData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Field;

import static com.example.truyentranhhapdan.activity.Fragment_tusach.adaptertruyen;
import static com.example.truyentranhhapdan.activity.Fragment_tusach.arrayListTruyen;
import static com.example.truyentranhhapdan.activity.MainActivity.userschinh;

public class ChiTietTruyenTranh extends AppCompatActivity implements View.OnClickListener {
    private int idtruyen=0;
    private String tenTruyen="";
    private String tenTacGia="";
    private String hinhanh="";
    private String mota="";
    private int sochuong=0;
    private int idtheloai=0;
    private String tentheloai="";
    private int ratting=0;
    private ArrayList<ChuongTruyen> listchuong;
    AdapterChuong adapterChuong;

    private Toolbar toolbar;
    private ImageView imageView;
    private TextView txttacgia,txttheloai,txtsochuong,txttrangthai,txtmota,txtrt;
    private TextView txttg,txttl,txtsl,txttt,txtratting,trailler,dschuong;
    private ListView list_item;
    private Button btnthemtu,btndoctruyen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chi_tiet_truyen_tranh);
        AnhXa();
        getThongTinTruyen();
        ActionBar();
        getDataChuong();
        ClickItemChuong();
    }

    private void ClickItemChuong() {
        list_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent=new Intent(getApplicationContext(),TrangDocTruyen.class);
                intent.putExtra("thongtinchuong",listchuong.get(i));
                startActivity(intent);
            }
        });
    }

    private void getDataChuong() {
        final RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        String url="http://thietbiandroid.000webhostapp.com/getdatachuong.php";
        StringRequest stringRequest=new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d("AAA",response);
                try {
                    JSONArray jsonArray=new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        int idchuong=jsonObject.getInt("idchuong");
                        String tenchuong=jsonObject.getString("tenchuong");
                        int idtruyen=jsonObject.getInt("idtruyen");
                        ChuongTruyen chuong=new ChuongTruyen(idchuong,tenchuong,idtruyen);
                        Log.d("AAA",chuong.toString());
                        listchuong.add(chuong);
                        adapterChuong.notifyDataSetChanged();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("idtruyen", String.valueOf(idtruyen));
                Log.d("AAA",idtruyen+"");
                return params;
            }
        };
        requestQueue.add(stringRequest);

    }

    private void AnhXa() {
        btnthemtu=findViewById(R.id.btnthem);
        btndoctruyen=findViewById(R.id.btndoctruyen);
        btnthemtu.setOnClickListener(this);
        btndoctruyen.setOnClickListener(this);

        listchuong=new ArrayList<>();
        adapterChuong=new AdapterChuong(getApplicationContext(),listchuong);
        list_item=findViewById(R.id.list_item);
        list_item.setAdapter(adapterChuong);

        trailler=findViewById(R.id.trailer);
        dschuong=findViewById(R.id.dschuong);
        imageView=findViewById(R.id.imagehinhanh);
        toolbar=findViewById(R.id.toobalTruyen);
        txttacgia=findViewById(R.id.txttacgia);
        txttheloai=findViewById(R.id.txttheloai);
        txtsochuong=findViewById(R.id.txtsochuong);
        txttrangthai=findViewById(R.id.txttrangthai);
        txtrt=findViewById(R.id.txtratting);
        txtmota=findViewById(R.id.txtmota);


        txttg=findViewById(R.id.txttg);
        txttl=findViewById(R.id.txttl);
        txtsl=findViewById(R.id.txttlsc);
        txttt=findViewById(R.id.txttt);
        txtratting=findViewById(R.id.txtrt);

        Typeface typeface=Typeface.createFromAsset(getApplicationContext().getAssets(),"Pacifico-Regular.ttf");
        Typeface typeface1=Typeface.createFromAsset(getApplicationContext().getAssets(),"GochiHand-Regular.ttf");
        Typeface typeface2=Typeface.createFromAsset(getApplicationContext().getAssets(),"Aclonica-Regular.ttf");

        btnthemtu.setTypeface(typeface);
        btndoctruyen.setTypeface(typeface);

        trailler.setTypeface(typeface2);
        dschuong.setTypeface(typeface2);

        txttg.setTypeface(typeface);
        txttl.setTypeface(typeface);
        txtsl.setTypeface(typeface);
        txttt.setTypeface(typeface);
        txtratting.setTypeface(typeface);



        txttacgia.setTypeface(typeface);
        txttheloai.setTypeface(typeface);
        txtsochuong.setTypeface(typeface);
        txttrangthai.setTypeface(typeface);
        txtrt.setTypeface(typeface);
        txtmota.setTypeface(typeface1);
    }

    private void ActionBar(){
        toolbar.setTitle(tenTruyen);
        toolbar.setNavigationIcon(R.drawable.backicon);
        toolbar.setBackgroundResource(R.color.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void getThongTinTruyen() {
        TruyenTranh truyenTranh= (TruyenTranh) getIntent().getSerializableExtra("ThongTinTruyen");
        idtruyen=truyenTranh.getId();
        tenTruyen=truyenTranh.getTenTruyen();
        tenTacGia=truyenTranh.getTacgia();
        hinhanh=truyenTranh.getHinhanh();
        mota=truyenTranh.getMota();
        sochuong=truyenTranh.getSochuong();
        idtheloai=truyenTranh.getId();
        tentheloai=truyenTranh.getTentheloai();
        ratting= (int) truyenTranh.getRatting();



        String trangthai="Đang cập nhập";
        txttacgia.setText(tenTacGia);
        txtsochuong.setText(sochuong+"");
        txttrangthai.setText(trangthai);
        txtmota.setText(mota);
        txttheloai.setText(tentheloai);
        txtrt.setText(String.valueOf(ratting));
        Glide
                .with(getApplicationContext())
                .load(hinhanh)
                .centerCrop()
                .placeholder(R.drawable.loading)
                .error(R.drawable.error)
                .into(imageView);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnthem:
                PostDataTruyenTuSach();
                TruyenTranh truyenTranh=new TruyenTranh(idtruyen,tenTruyen,tenTacGia,
                        hinhanh,mota,sochuong,ratting,idtheloai,tentheloai);
//                sendatatoFragment_Tusach(truyenTranh);
                break;
            case R.id.btndoctruyen:
                break;
        }
    }

    private void sendatatoFragment_Tusach(TruyenTranh truyenTranh) {
        arrayListTruyen.add(truyenTranh);
        adaptertruyen.setData(arrayListTruyen);
    }


    private void PostDataTruyenTuSach() {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                //Starting Write and Read data with URL
                //Creating array for parameters
                String[] field = new String[10];
                field[0] = "idtruyen";
                field[1] = "tentruyen";
                field[2] = "tacgia";
                field[3] = "hinhanh";
                field[4] = "Mota";
                field[5] = "sochuong";
                field[6] = "ratting";
                field[7] = "idtheloai";
                field[8] = "tentheloai";
                field[9] = "iduser";

                //Creating array for data
                String[] data = new String[10];
                data[0] = String.valueOf(idtruyen);
                data[1] = tenTruyen;
                data[2] = tenTacGia;
                data[3] = hinhanh;
                data[4] = mota;
                data[5] = String.valueOf(sochuong);
                data[6] = String.valueOf(ratting);
                data[7] = String.valueOf(idtheloai);
                data[8] = tentheloai;
                data[9] = String.valueOf(userschinh.getId());
                PutData putData = new PutData("https://thietbiandroid.000webhostapp.com/postdatatusach.php", "POST", field, data);
                if (putData.startPut()) {
                    if (putData.onComplete()) {
                        String result = putData.getResult();
                        Log.d("LOG",result);
                        if (result.equals("Success")){
                            Toast.makeText(getApplicationContext(),"Thêm thành công",Toast.LENGTH_LONG).show();
                        }
                        else if(result.equals("Failed")||result.equals("All fields are required")){
                            Toast.makeText(getApplicationContext(),"Thêm thất bại",Toast.LENGTH_LONG).show();
                        }
                        else
                          Toast.makeText(getApplicationContext(),"Bạn đã thêm từ lâu rồi",Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }
}