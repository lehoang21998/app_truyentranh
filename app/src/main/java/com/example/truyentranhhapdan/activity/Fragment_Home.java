package com.example.truyentranhhapdan.activity;

import android.graphics.Typeface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.truyentranhhapdan.R;
import com.example.truyentranhhapdan.adapter.Adaptertheloai;
import com.example.truyentranhhapdan.adapter.Adaptertruyen;
import com.example.truyentranhhapdan.adapter.adapterviewpagerphotos;
import com.example.truyentranhhapdan.model.Photoviewpager;
import com.example.truyentranhhapdan.model.TruyenTranh;
import com.example.truyentranhhapdan.model.theloai;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;


public class Fragment_Home extends Fragment {
    private View view;
    private EditText editTimkiem;
    private ViewPager viewPager;
    private CircleIndicator circleIndicator;
    private adapterviewpagerphotos adapterviewpagerphotos;
    private RecyclerView recyclerView;
    private Adaptertheloai adaptertheloai;
    private ArrayList<theloai> arrayListtheloai;
    private ArrayList<TruyenTranh> listtruyenmoi;
    private RecyclerView recyclerViewtruyenmoi;
    private Adaptertruyen adaptertruyen;
    private TextView txtTruyenmoi;

    private Timer timer;
    private List<Photoviewpager> mListPhoto;

    public Fragment_Home() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("HASTAG", " HOME ON REUSUME");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("HASTAG", " HOME ON onPause");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view= inflater.inflate(R.layout.fragment__home, container, false);
        txtTruyenmoi=view.findViewById(R.id.txtTruyenmoi);
        Typeface typeface=Typeface.createFromAsset(getActivity().getAssets(),"Pacifico-Regular.ttf");
        txtTruyenmoi.setTypeface(typeface);
        AnhXa(view);
        mListPhoto=getListPhoto();
        GetDataTruyenMoi(view);
        GetDataTruyen(view);
        autoSlideImages(view);
        return view;
    }

    private void autoSlideImages(View view) {
        if(mListPhoto==null ||mListPhoto.isEmpty()||viewPager==null)
            return;
        if(timer==null)
            timer=new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        int vitriphoto=viewPager.getCurrentItem();
                        int totalviewpager=mListPhoto.size()-1;
                        if(vitriphoto<totalviewpager){
                            vitriphoto++;
                            viewPager.setCurrentItem(vitriphoto);
                        }else {
                            viewPager.setCurrentItem(0);
                        }
                    }
                });
            }
        },500,3000);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(timer!=null){
            timer.cancel();
            timer=null;
        }
    }

    private void AnhXa(View view) {
        //List truyện mới
        listtruyenmoi=new ArrayList<>();
        recyclerViewtruyenmoi=view.findViewById(R.id.recleviewtruyenmoi);
        recyclerViewtruyenmoi.setHasFixedSize(true);
        recyclerViewtruyenmoi.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false));
        adaptertruyen=new Adaptertruyen(getContext(),R.layout.item_truyen_tranh);
        adaptertruyen.setData(listtruyenmoi);
        recyclerViewtruyenmoi.setAdapter(adaptertruyen);



        editTimkiem=view.findViewById(R.id.edit_timkiem);
        viewPager=view.findViewById(R.id.viewpager);
        circleIndicator=view.findViewById(R.id.circle);


        //List Thể loại
        arrayListtheloai=new ArrayList<>();
        recyclerView=view.findViewById(R.id.recleview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        adaptertheloai=new Adaptertheloai(getContext());
        adaptertheloai.setData(arrayListtheloai);
        recyclerView.setAdapter(adaptertheloai);


        // slide image
        adapterviewpagerphotos=new adapterviewpagerphotos(getContext(),getListPhoto());
        viewPager.setAdapter(adapterviewpagerphotos);
        circleIndicator.setViewPager(viewPager);
        adapterviewpagerphotos.registerDataSetObserver(circleIndicator.getDataSetObserver());
        

    }

    private void GetDataTruyenMoi(View view) {
        final RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        String url="http://thietbiandroid.000webhostapp.com/gettruyenmoi.php";
        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray=new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++){
                        JSONObject jsonObjecttruyen=jsonArray.getJSONObject(i);
                        int idtruyen=jsonObjecttruyen.getInt("id");
                        String tentruyen=jsonObjecttruyen.getString("tentruyen");
                        String tacgia=jsonObjecttruyen.getString("tacgia");
                        String hinhanh=jsonObjecttruyen.getString("hinhanh");
                        String mota=jsonObjecttruyen.getString("mota");
                        int sochuong=jsonObjecttruyen.getInt("sochuong");
                        long ratting=jsonObjecttruyen.getLong("ratting");
                        int idtheloai=jsonObjecttruyen.getInt("idtheloai");
                        String tentheloai=jsonObjecttruyen.getString("tentheloai");
                        TruyenTranh truyenTranh=new TruyenTranh(idtruyen,tentruyen,tacgia,hinhanh,mota,sochuong,ratting,idtheloai,tentheloai);
                        listtruyenmoi.add(truyenTranh);
                        adaptertruyen.setData(listtruyenmoi);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);
    }

    private void GetDataTruyen(View view){
        final RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        String url="http://thietbiandroid.000webhostapp.com/getdatatruyen.php";
        StringRequest stringRequest=new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray=new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++){
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        int idtheloai=jsonObject.getInt("id");
                        String tentheloai=jsonObject.getString("theloai");
                        JSONArray arrayList=jsonObject.getJSONArray("danhsach");
                        ArrayList<TruyenTranh> ListTruyen=new ArrayList<>();
                        for (int j=0;j<arrayList.length();j++){
                            JSONObject jsonObjecttruyen=arrayList.getJSONObject(j);
                            int idtruyen=jsonObjecttruyen.getInt("id");
                            String tentruyen=jsonObjecttruyen.getString("tentruyen");
                            String tacgia=jsonObjecttruyen.getString("tacgia");
                            String hinhanh=jsonObjecttruyen.getString("hinhanh");
                            String mota=jsonObjecttruyen.getString("mota");
                            int sochuong=jsonObjecttruyen.getInt("sochuong");
                            long ratting=jsonObjecttruyen.getLong("ratting");
                            int idtheloaai=jsonObjecttruyen.getInt("idtheloai");
                            TruyenTranh truyenTranh=new TruyenTranh(idtruyen,tentruyen,tacgia,hinhanh,mota,sochuong,ratting,idtheloaai,tentheloai);
                            ListTruyen.add(truyenTranh);
                        }
                        theloai theloai=new theloai(idtheloai,tentheloai,ListTruyen);
                        arrayListtheloai.add(theloai);
                        adaptertheloai.setData(arrayListtheloai);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        requestQueue.add(stringRequest);

    }

    private List<Photoviewpager> getListPhoto() {
        List<Photoviewpager> list=new ArrayList<>();
        list.add(new Photoviewpager("http://st.truyenchon.com/data/comics/127/toan-chuc-phap-su.jpg"));
        list.add(new Photoviewpager("http://st.truyenchon.com/data/comics/214/van-gioi-tien-tung.jpg"));
        list.add(new Photoviewpager("http://st.truyenchon.com/data/comics/48/cuu-duong-de-ton.jpg"));
        list.add(new Photoviewpager("http://st.truyenchon.com/data/comics/58/cung-chieu-dao-phi.jpg"));
        list.add(new Photoviewpager("http://st.truyenchon.com/data/comics/132/bien-nien-su-cua-thien-quy.jpg"));
        return  list;
    }


}