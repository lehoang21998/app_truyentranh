package com.example.truyentranhhapdan.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.example.truyentranhhapdan.R;
import com.example.truyentranhhapdan.adapter.AdapterTrangTruyen;
import com.example.truyentranhhapdan.demo.MainInterface;
import com.example.truyentranhhapdan.model.ChuongTruyen;
import com.example.truyentranhhapdan.model.TrangTruyen;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class TrangDocTruyen extends AppCompatActivity {
    private RecyclerView recyclerView;
    private LinearLayout linearLayout;
    private ProgressBar progressBar;
    private NestedScrollView nestedScrollView;

    private int page=1;
    private int limit=5;
    private int idchuong=1;
    AdapterTrangTruyen adapterTrangTruyen;
    ArrayList<TrangTruyen> arrayListTrang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trang_doc_truyen);
        Anhxa();
        getDataChuong(page,limit);
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(scrollY==v.getChildAt(0).getMeasuredHeight()-v.getMeasuredHeight()){
                    page++;
                    linearLayout.setVisibility(View.VISIBLE);
                    getDataChuong(page,limit);
                }
            }
        });
    }

    private void getDataChuong( int page, int limit) {
        Retrofit retrofit=new Retrofit.Builder()
                .baseUrl("http://thietbiandroid.000webhostapp.com/")
                .addConverterFactory(ScalarsConverterFactory.create()).build();
        MainInterface mainInterface=retrofit.create(MainInterface.class);
        idchuong=GetIdChuong();
        Call<String> call =mainInterface.STRING_CALLS(page,limit,idchuong);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()&&response.body()!=null){
                    linearLayout.setVisibility(View.GONE);
                    Log.d("LOG",response.body().toString());
                    try {
                        JSONArray jsonArray=new JSONArray(response.body());
                        parseResulf(jsonArray);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("LOG",t.getMessage());
            }
        });
    }

    private void parseResulf(JSONArray jsonArray) {
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                int idtrang = jsonObject.getInt("idtrang");
                String hinhanh = jsonObject.getString("hinhanh");
                int idchuong = jsonObject.getInt("idchuong");
                TrangTruyen trangTruyen=new TrangTruyen(idtrang,hinhanh,idchuong);
                arrayListTrang.add(trangTruyen);
                adapterTrangTruyen.setData(arrayListTrang);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }
    private int GetIdChuong() {
        ChuongTruyen chuongTruyen= (ChuongTruyen) getIntent().getSerializableExtra("thongtinchuong");
        return chuongTruyen.getIdchuong();
    }

    private void Anhxa() {
        nestedScrollView=findViewById(R.id.nesscoll);
        recyclerView=findViewById(R.id.recleviewtruyentrang);
        linearLayout=findViewById(R.id.line1trang);
        progressBar=findViewById(R.id.progress_circular_trang);

        arrayListTrang=new ArrayList<>();
        adapterTrangTruyen=new AdapterTrangTruyen(getApplicationContext(),R.layout.image_hinhanh_truyen);
        adapterTrangTruyen.setData(arrayListTrang);
        RecyclerView.ItemDecoration itemDecoration=new DividerItemDecoration(this,DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecoration);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapterTrangTruyen);

    }
}