package com.example.truyentranhhapdan.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.truyentranhhapdan.R;
import com.example.truyentranhhapdan.Util;
import com.example.truyentranhhapdan.model.Users;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.vishnusivadas.advanced_httpurlconnection.PutData;

public class SignIn extends AppCompatActivity implements View.OnClickListener {
    private CheckBox checkBoxSaveActor;
    private TextInputEditText edit_tk,edit_pass;
    private Button btnSignIn;
    private ProgressBar progressBar;
    private TextView txtSignUp,txtThongBao;

    private String tentk,pass;
    public  static   String username,password;
    private Boolean mcheck=false;


    SharedPreferences tenTaiKhoan,Password,isChecked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        AnhXa();
        tenTaiKhoan=getSharedPreferences("tenTaiKhoan",MODE_PRIVATE);
        Password=getSharedPreferences("Password",MODE_PRIVATE);
        isChecked=getSharedPreferences("isChecked",MODE_PRIVATE);

        mcheck=isChecked.getBoolean("checked",false);
        checkBoxSaveActor.setChecked(mcheck);
        Log.d("CHECK",mcheck.toString());

        if(mcheck==false){
            edit_tk.setText("");
            edit_pass.setText("");
        }else{
            tentk=tenTaiKhoan.getString("taikhoan","");
            edit_tk.setText(tentk);
            pass=Password.getString("password","");
            edit_pass.setText(pass);
        }




    }
    private void AnhXa() {
        checkBoxSaveActor=findViewById(R.id.checkboxSignIn);
        edit_tk=findViewById(R.id.edit_tentaikhoan);
        edit_pass=findViewById(R.id.edit_pass);
        btnSignIn=findViewById(R.id.btnSignIn);
        txtSignUp=findViewById(R.id.txtSignUp);
        progressBar=findViewById(R.id.progress_cirSignIn);
        btnSignIn.setOnClickListener(this);
        txtSignUp.setOnClickListener(this);
        txtThongBao=findViewById(R.id.txtThongbao);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnSignIn:

                username=edit_tk.getText().toString();
                password=edit_pass.getText().toString();


                if( !username.equals("") && !password.equals("")  ){
                    progressBar.setVisibility(View.VISIBLE);
                    Handler handler = new Handler(Looper.getMainLooper());
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            //Starting Write and Read data with URL
                            //Creating array for parameters
                            String[] field = new String[2];

                            field[0] = "username";
                            field[1] = "password";

                            //Creating array for data
                            String[] data = new String[2];

                            data[0] = username;
                            data[1] = password;

                            PutData putData = new PutData("https://thietbiandroid.000webhostapp.com/login.php", "POST", field, data);
                            if (putData.startPut()) {
                                if (putData.onComplete()) {
                                    progressBar.setVisibility(View.GONE);
                                    String result = putData.getResult();
                                    result =result.substring(1,result.length()-1);
                                    Log.d("AAA",result);
                                    Util util=new Util();
                                    if (util.isJSONValid(result)){
                                        Gson gson = new Gson();
                                        Users users = gson.fromJson(result, Users.class);
                                        Log.d("AAA",users.getFullname());
                                        Intent intent=new Intent(getApplicationContext(), MainActivity.class);
                                        intent.putExtra("thongtindocgia",users);
                                        startActivity(intent);
                                        finish();
                                    }else{
                                        txtThongBao.setVisibility(View.VISIBLE);
                                        txtThongBao.setText("Đăng nhập thất bại,vui lòng kiểm tra lại tài khoản hoặc mật khẩu");
                                    }
                                }
                            }
                            //End Write and Read data with URL
                        }
                    });
                }
                else {
                    txtThongBao.setVisibility(View.VISIBLE);
                    txtThongBao.setText(" Không được bỏ trống thông tin nào");
                }
                SharedPreferences.Editor editorchecked=isChecked.edit();
                if(checkBoxSaveActor.isChecked()){
                    editorchecked.putBoolean("checked",true);
                    LuuThongTinUser();
                }
                else  {
                    editorchecked.putBoolean("checked",false);
                }
                editorchecked.commit();
                break;
            case R.id.txtSignUp:
                Intent intent=new Intent(getApplicationContext(),SignUp.class);
                startActivity(intent);
                finish();
                break;
        }
    }

    private void LuuThongTinUser(){
        SharedPreferences.Editor editor=tenTaiKhoan.edit();
        SharedPreferences.Editor editorpass=Password.edit();


        editor.putString("taikhoan",edit_tk.getText().toString());
        editorpass.putString("password",edit_pass.getText().toString());

        editor.commit();
        editorpass.commit();
    }
}