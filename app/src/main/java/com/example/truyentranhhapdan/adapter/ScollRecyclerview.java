package com.example.truyentranhhapdan.adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class ScollRecyclerview  extends RecyclerView.OnScrollListener {
    private LinearLayoutManager linearLayoutManager;

    public ScollRecyclerview(LinearLayoutManager linearLayoutManager) {
        this.linearLayoutManager = linearLayoutManager;
    }

    @Override
    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        int visibleitemcount=linearLayoutManager.getChildCount();
        int totalitemcount=linearLayoutManager.getItemCount();
        int fistVisible=linearLayoutManager.findFirstVisibleItemPosition();
        if(isloading() || islastpage()){
            return;
        }
        if(fistVisible>=0 && (visibleitemcount+fistVisible) >= totalitemcount){
            Loadmoreitem();
        }
    }
    public  abstract  void Loadmoreitem();
    public abstract boolean isloading();
    public abstract boolean islastpage();
}
