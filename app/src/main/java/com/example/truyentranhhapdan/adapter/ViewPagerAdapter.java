package com.example.truyentranhhapdan.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.truyentranhhapdan.activity.Fragment_Docgia;
import com.example.truyentranhhapdan.activity.Fragment_Home;
import com.example.truyentranhhapdan.activity.Fragment_tusach;
import com.example.truyentranhhapdan.activity.Fragment_khampha;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    public ViewPagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new Fragment_Home();
            case 1:
                return new Fragment_tusach();
            case 2:
                return new Fragment_khampha();
            case 3:
                return new Fragment_Docgia();
            default:
                return new Fragment_Home();
        }
    }

    @Override
    public int getCount() {
        return 4;
    }
}
