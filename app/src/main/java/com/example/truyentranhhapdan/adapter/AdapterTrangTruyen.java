package com.example.truyentranhhapdan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.truyentranhhapdan.R;
import com.example.truyentranhhapdan.model.TrangTruyen;

import java.util.ArrayList;

public class AdapterTrangTruyen extends RecyclerView.Adapter<AdapterTrangTruyen.ViewHodel> {
    private Context context;
    private ArrayList<TrangTruyen> listTrang;
    private int layout;

    public AdapterTrangTruyen(Context context, int layout) {
        this.context = context;
        this.layout = layout;
    }
    public void setData(ArrayList<TrangTruyen> array){
        this.listTrang=array;
        notifyDataSetChanged();
    }



    @NonNull
    @Override
    public ViewHodel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(parent.getContext()).inflate(layout,parent,false);
        return new ViewHodel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHodel holder, int position) {
        final TrangTruyen trangTruyen=listTrang.get(position);
        if(trangTruyen==null)return;
        else{
            Glide
                    .with(context)
                    .load(trangTruyen.getHinhanh())
                    .centerInside()
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.error)
                    .into(holder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        if(listTrang!=null)
            return listTrang.size();
        else
            return 0;
    }

    public class ViewHodel extends RecyclerView.ViewHolder{
        private ImageView imageView;
        public ViewHodel(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.idimage);
        }
    }

}
