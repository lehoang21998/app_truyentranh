package com.example.truyentranhhapdan.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.example.truyentranhhapdan.R;
import com.example.truyentranhhapdan.model.Photoviewpager;

import java.util.List;

public class adapterviewpagerphotos extends PagerAdapter {
    private Context context;
    private List<Photoviewpager> listphoto;

    public adapterviewpagerphotos(Context context, List<Photoviewpager> listphoto) {
        this.context = context;
        this.listphoto = listphoto;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View view= LayoutInflater.from(container.getContext()).inflate(R.layout.item_photo,container,false);
        ImageView imageView=view.findViewById(R.id.image);
        Photoviewpager photoviewpager=listphoto.get(position);
        if (photoviewpager!=null){
            Glide.with(context)
                    .load(photoviewpager.getResourc_photo()).into(imageView);
        }
        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        if (listphoto!=null) return listphoto.size();
       return 0;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view==object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
