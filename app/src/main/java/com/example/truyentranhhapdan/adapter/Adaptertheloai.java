package com.example.truyentranhhapdan.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.truyentranhhapdan.R;
import com.example.truyentranhhapdan.Util;
import com.example.truyentranhhapdan.activity.TrangCuaTheLoai;
import com.example.truyentranhhapdan.model.TruyenTranh;
import com.example.truyentranhhapdan.model.theloai;

import java.util.ArrayList;

import static com.example.truyentranhhapdan.R.id.txttheloai;

public class Adaptertheloai extends RecyclerView.Adapter<Adaptertheloai.ViewHodel> {
    Context context;
    ArrayList<theloai> arrayListtheloai;

    public Adaptertheloai(Context context) {
        this.context = context;
    }
    public void setData(ArrayList<theloai> arrayListtheloai){
        this.arrayListtheloai=arrayListtheloai;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public ViewHodel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_theloai_home_vertical,parent,false);
        return new ViewHodel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHodel holder, int position) {
        final theloai theloai=arrayListtheloai.get(position);
        if (theloai==null)return;
        else{
           holder.title.setText(theloai.getTenTheLoai());
           ArrayList<TruyenTranh> arrayList=theloai.getArrayListtruyen();
            Adaptertruyen adaptertruyen=new Adaptertruyen(context,R.layout.item_truyen_tranh);
            adaptertruyen.setData(arrayList);
            holder.recyclerView.setHasFixedSize(true);
            holder.recyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false));
            holder.recyclerView.setAdapter(adaptertruyen);
            holder.btnxemthem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, TrangCuaTheLoai.class);
                    intent.putExtra("ThongTinTheLoai",theloai);
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if(arrayListtheloai!=null)
            return arrayListtheloai.size();
        else
            return 0;
    }

    public class  ViewHodel extends RecyclerView.ViewHolder{
        private TextView title;
        private TextView btnxemthem;
        private RecyclerView recyclerView;
        public ViewHodel(@NonNull View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.txttheloai);
            btnxemthem=itemView.findViewById(R.id.btnxemthem);
            recyclerView=itemView.findViewById(R.id.recycleviewhorizotal);
            Typeface typeface=Typeface.createFromAsset(context.getAssets(),"Pacifico-Regular.ttf");
            title.setTypeface(typeface);
            btnxemthem.setTypeface(typeface);


        }
    }
}
