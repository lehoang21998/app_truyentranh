package com.example.truyentranhhapdan.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.truyentranhhapdan.R;
import com.example.truyentranhhapdan.model.ChuongTruyen;

import java.util.List;

public class AdapterChuong extends BaseAdapter {
    private Context context;
    List<ChuongTruyen> lisChuong;

    public AdapterChuong(Context context, List<ChuongTruyen> lisChuong) {
        this.context = context;
        this.lisChuong = lisChuong;
    }

    @Override
    public int getCount() {
        return lisChuong.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }
    //class viewhodel

    private class ViewHolder{
        private TextView txtidchuong,txttenchuong;
        private LinearLayout linearLayout;
    }


    @SuppressLint("SetTextI18n")
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        //dinh nghĩa lay out cần tìm
        if(view==null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.item_chuong, null);
            holder=new ViewHolder();
            //anh xạ
            holder.txtidchuong = view.findViewById(R.id.txtid);
            holder.txttenchuong = view.findViewById(R.id.txtten);
            holder.linearLayout = view.findViewById(R.id.item_chuong);
            Typeface typeface1=Typeface.createFromAsset(context.getAssets(),"GochiHand-Regular.ttf");
            holder.txtidchuong.setTypeface(typeface1);
            holder.txttenchuong.setTypeface(typeface1);
            view.setTag(holder);
        }else{
            holder= (ViewHolder) view.getTag();
        }
        // gán giá trị
        ChuongTruyen chuongTruyen=lisChuong.get(i);
        holder.txtidchuong.setText("Chương "+i);
        holder.txttenchuong.setText(chuongTruyen.getTenChuong());
        if(i%2==0) {
            holder.linearLayout.setBackgroundResource(R.drawable.custom_item_chuong);
        }
        else {
            holder.linearLayout.setBackgroundResource(R.drawable.custom_item_chuong1);
        }
        return view;
    }
}
