package com.example.truyentranhhapdan.adapter;

import android.content.Context;
import android.content.Intent;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.truyentranhhapdan.R;
import com.example.truyentranhhapdan.activity.ChiTietTruyenTranh;
import com.example.truyentranhhapdan.model.TruyenTranh;
import com.example.truyentranhhapdan.model.theloai;

import java.util.ArrayList;

public class Adaptertruyen extends RecyclerView.Adapter<Adaptertruyen.ViewHodel> {
    Context context;
    ArrayList<TruyenTranh> arrayListtruyen;
    int layout;

    public Adaptertruyen(Context context, int layout) {
        this.context = context;
        this.layout = layout;
    }

    public void setData(ArrayList<TruyenTranh> array){
        this.arrayListtruyen=array;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ViewHodel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(layout,parent,false);

        return new ViewHodel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHodel holder, int position) {
        final TruyenTranh truyenTranh=arrayListtruyen.get(position);
        if(truyenTranh==null)return;
        else{
            holder.textView.setText(truyenTranh.getTenTruyen()+"\nChapter "+truyenTranh.getSochuong());
            holder.txtratting.setText(String.valueOf(truyenTranh.getRatting()));
            Glide
                    .with(context)
                    .load(truyenTranh.getHinhanh())
                    .centerCrop()
                    .placeholder(R.drawable.loading)
                    .error(R.drawable.error)
                    .into(holder.imageView);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(context, ChiTietTruyenTranh.class);
                    intent.putExtra("ThongTinTruyen",truyenTranh);
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if(arrayListtruyen!=null)
            return arrayListtruyen.size();
        else
            return 0;
    }

    public class ViewHodel extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView textView,txtratting;
        public ViewHodel(@NonNull View itemView) {
            super(itemView);
            imageView=itemView.findViewById(R.id.idimage);
            textView=itemView.findViewById(R.id.txttentruyen);
            txtratting=itemView.findViewById(R.id.txtratting);
        }
    }
}
