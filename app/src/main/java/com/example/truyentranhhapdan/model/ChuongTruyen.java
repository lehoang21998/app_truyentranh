package com.example.truyentranhhapdan.model;

import android.graphics.drawable.Drawable;

import java.io.Serializable;

public class ChuongTruyen implements Serializable {
    private int idchuong;
    private String tenChuong;
    private int idtruyen;


    public ChuongTruyen(int idchuong, String tenChuong, int idtruyen) {
        this.idchuong = idchuong;
        this.tenChuong = tenChuong;
        this.idtruyen = idtruyen;
    }


    @Override
    public String toString() {
        return "ChuongTruyen{" +
                "idchuong=" + idchuong +
                ", tenChuong='" + tenChuong + '\'' +
                ", idtruyen=" + idtruyen +
                '}';
    }

    public int getIdchuong() {
        return idchuong;
    }

    public void setIdchuong(int idchuong) {
        this.idchuong = idchuong;
    }

    public String getTenChuong() {
        return tenChuong;
    }

    public void setTenChuong(String tenChuong) {
        this.tenChuong = tenChuong;
    }

    public int getIdtruyen() {
        return idtruyen;
    }

    public void setIdtruyen(int idtruyen) {
        this.idtruyen = idtruyen;
    }

}
