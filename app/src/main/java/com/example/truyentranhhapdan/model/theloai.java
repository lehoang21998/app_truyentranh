package com.example.truyentranhhapdan.model;

import java.io.Serializable;
import java.util.ArrayList;

public class theloai implements Serializable {
    private int idtheloai;
    private String tenTheLoai;
    private ArrayList<TruyenTranh> arrayListtruyen;

    public theloai(int idtheloai, String tenTheLoai, ArrayList<TruyenTranh> arrayListtruyen) {
        this.idtheloai = idtheloai;
        this.tenTheLoai = tenTheLoai;
        this.arrayListtruyen = arrayListtruyen;
    }

    public int getIdtheloai() {
        return idtheloai;
    }

    public void setIdtheloai(int idtheloai) {
        this.idtheloai = idtheloai;
    }

    public String getTenTheLoai() {
        return tenTheLoai;
    }

    public void setTenTheLoai(String tenTheLoai) {
        this.tenTheLoai = tenTheLoai;
    }

    public ArrayList<TruyenTranh> getArrayListtruyen() {
        return arrayListtruyen;
    }

    public void setArrayListtruyen(ArrayList<TruyenTranh> arrayListtruyen) {
        this.arrayListtruyen = arrayListtruyen;
    }
}
