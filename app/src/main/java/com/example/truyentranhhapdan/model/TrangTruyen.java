package com.example.truyentranhhapdan.model;

public class TrangTruyen {
    private int idtrang;
    private String hinhanh;
    private int idchuong;

    public TrangTruyen(int idtrang, String hinhanh, int idchuong) {
        this.idtrang = idtrang;
        this.hinhanh = hinhanh;
        this.idchuong = idchuong;
    }

    public int getIdtrang() {
        return idtrang;
    }

    public void setIdtrang(int idtrang) {
        this.idtrang = idtrang;
    }

    public String getHinhanh() {
        return hinhanh;
    }

    public void setHinhanh(String hinhanh) {
        this.hinhanh = hinhanh;
    }

    public int getIdchuong() {
        return idchuong;
    }

    public void setIdchuong(int idchuong) {
        this.idchuong = idchuong;
    }
}
