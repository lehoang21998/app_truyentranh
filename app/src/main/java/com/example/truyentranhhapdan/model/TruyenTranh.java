package com.example.truyentranhhapdan.model;

import java.io.Serializable;

public class TruyenTranh implements Serializable {
    private int id;
    private String tenTruyen;
    private String tacgia;
    private String hinhanh;
    private String Mota;
    private int sochuong;
    private long ratting;
    private int idtheloai;
    private String tentheloai;

    public TruyenTranh(int id, String tenTruyen, String tacgia, String hinhanh, String mota, int sochuong, long ratting, int idtheloai, String tentheloai) {
        this.id = id;
        this.tenTruyen = tenTruyen;
        this.tacgia = tacgia;
        this.hinhanh = hinhanh;
        Mota = mota;
        this.sochuong = sochuong;
        this.ratting = ratting;
        this.idtheloai = idtheloai;
        this.tentheloai = tentheloai;
    }

    public String getTentheloai() {
        return tentheloai;
    }

    public void setTentheloai(String tentheloai) {
        this.tentheloai = tentheloai;
    }

    public long getRatting() {
        return ratting;
    }

    public void setRatting(long ratting) {
        this.ratting = ratting;
    }

    public String getHinhanh() {
        return hinhanh;
    }

    public void setHinhanh(String hinhanh) {
        this.hinhanh = hinhanh;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenTruyen() {
        return tenTruyen;
    }

    public void setTenTruyen(String tenTruyen) {
        this.tenTruyen = tenTruyen;
    }

    public String getTacgia() {
        return tacgia;
    }

    public void setTacgia(String tacgia) {
        this.tacgia = tacgia;
    }

    public String getMota() {
        return Mota;
    }

    public void setMota(String mota) {
        Mota = mota;
    }

    public int getSochuong() {
        return sochuong;
    }

    public void setSochuong(int sochuong) {
        this.sochuong = sochuong;
    }

    public int getIdtheloai() {
        return idtheloai;
    }

    public void setIdtheloai(int idtheloai) {
        this.idtheloai = idtheloai;
    }
}
